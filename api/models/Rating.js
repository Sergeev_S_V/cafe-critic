const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const RatingSchema = new Schema({
  food: {
    type: Number,
    default: 0
  },
  service: {
    type: Number,
    default: 0
  },
  interior: {
    type: Number,
    default: 0
  },
  overall: {
    type: Number,
    default: 0
  }
});

RatingSchema.set('toJSON', {
  transform: (doc, ret, options) => {
    delete ret._id;
    return ret;
  }
});

module.exports = RatingSchema;