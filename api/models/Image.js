const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const ImageSchema = new Schema({
  image: {
    type: String,
    required: true
  },
  place: {
    type: Schema.Types.ObjectId,
    ref: 'Place'
  },
  user: {
    type: Schema.Types.ObjectId,
    ref: 'User'
  }
});

const Image = mongoose.model('Image', ImageSchema);

module.exports = Image;