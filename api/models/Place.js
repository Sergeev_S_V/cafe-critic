const mongoose = require('mongoose');
const RatingSchema = require('./Rating');
const Schema = mongoose.Schema;

const PlaceSchema = new Schema({
  user: {
    type: Schema.Types.ObjectId,
    ref: 'User'
  },
  title: {
    type: String,
    required: true
  },
  description: {
    type: String,
    required: true
  },
  photo: {
    type: String,
    required: true
  },
  rating: RatingSchema,
  images: [{
    type: Schema.Types.ObjectId,
    ref: 'Image'
  }],
  reviews: [{
    type: Schema.Types.ObjectId,
    ref: 'Review'
  }]
});

const Place = mongoose.model('Place', PlaceSchema);

module.exports = Place;