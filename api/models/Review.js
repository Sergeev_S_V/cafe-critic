const mongoose = require('mongoose');

const Schema = mongoose.Schema;
const RatingSchema = require('./Rating');

const ReviewSchema = new Schema({
  date: {
    type: String,
    required: true
  },
  place: {
    type: Schema.Types.ObjectId,
    ref: 'Place'
  },
  user: {
    type: Schema.Types.ObjectId,
    ref: 'User'
  },
  review: {
    type: String,
    required: true
  },
  rating: RatingSchema,
});

const Review = mongoose.model('Review', ReviewSchema);

module.exports = Review;