const express = require('express');
const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');

const auth = require('../middleware/auth');
const permit = require('../middleware/permit');
const config = require('../config');
const Image = require('../models/Image');
const Place = require('../models/Place');

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});
const upload = multer({storage});

const createRouter = () => {
  const router = express.Router();

  router.post('/', [auth, upload.single('image')], async (req, res) => {
    const placeId = req.body.placeId;

    const imageData = {
      image: req.file.filename,
      place: placeId,
      user: req.user._id,
    };

    const image = new Image(imageData);

    try {
      const savedImage = await image.save();
      const place = await Place.findById(placeId);
      place.images.push(savedImage._id);
      await place.save();
      return res.status(200).send({image, message: 'Картинка успешно загружена!'});
    } catch (error) {
    	return res.status(400).send({error: 'Ошибка в запросе, фото не загрузилось'});
    }
  });

  router.delete('/:id', [auth, permit('admin')], async (req, res) => {
    try {
      const imageId = req.params.id;
      await Image.findByIdAndDelete(imageId);
      await Place.update({}, {$pull: {images: {$in: imageId}}});
      const images = await Image.find();
      return res.status(200).send({images, message: 'Картинка успешно удалена!'});
    } catch (error) {
    	return res.status(500).send({error: 'Ошибка при удалении, попробуйте позже'});
    }
  });

  return router;
};

module.exports = createRouter;
