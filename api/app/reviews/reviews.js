const moment = require('moment');
const express = require('express');

const auth = require('../../middleware/auth');
const Review = require('../../models/Review');
const Place = require('../../models/Place');
const count = require('./functions');

const createRouter = () => {
  const router = express.Router();

  router.post('/', auth, async (req, res) => {
    const { placeId, review, food, service, interior } = req.body;

    try {
      const review = await Review.findOne({user: req.user._id, place: placeId});
      if (review) {
        return res.status(403).send({message: 'Вы уже оставили отзыв об этом заведении, чтобы сделать это снова, нужно удалить предыдущий', reviewId: review._id});
      }
    } catch (e) {
      return res.status(400).send({message: 'Неверный запрос, попробуйте позже'});
    }

    const reviewData = {
      date: moment().format("DD.MM.YYYY в HH:mm"),
      place: placeId,
      user: req.user._id,
      review: review,
    };

    const ratingData = {
      food: food,
      service: service,
      interior: interior
    };
    const newReview = new Review(reviewData);
    newReview.rating = ratingData;
    const savedReview = await newReview.save();

    const place = await Place.findById(placeId);
    place.reviews.push(savedReview._id);
    const savedPlace = await place.save();

    const foundPlace = await Place.findById(savedPlace._id)
      .populate('images')
      .populate({path: 'reviews', populate: {path: 'user'}});
    const reviews = foundPlace.reviews;
    const amountReviews = reviews.length;

    const sumInterior = reviews.reduce((acc, review) => acc + review.rating.interior, 0);
    const sumService = reviews.reduce((acc, review) => acc + review.rating.service, 0);
    const sumFood = reviews.reduce((acc, review) => acc + review.rating.food, 0);
    const averageInterior = sumInterior === 0 ? 0 : sumInterior / amountReviews;
    const averageService = sumService === 0 ? 0 : sumService / amountReviews;
    const averageFood = sumFood === 0 ? 0 : sumFood / amountReviews;


    foundPlace.rating = {
      interior: averageInterior,
      service: averageService,
      food: averageFood,
      overall: ((averageInterior + averageService + averageFood) / 3).toFixed(1)
    };
    await foundPlace.save();

    return res.status(200).send({place: foundPlace});
  });

  router.delete('/:id', async (req, res) => {
    try {
      const reviewId = req.params.id;
      const review = await Review.findById(reviewId);
      await review.remove();
      const place = await Place
        .findOneAndUpdate({_id: review.place}, {$pull: {reviews: {$in: reviewId}}})
        .populate('reviews');

      const copyReviews = place.reviews;
      const amountReviews = copyReviews.length;

      // const sum = count.sumRatings(copyReviews);
      // const averageRating = count.averageRating(sum, amountReviews);

      const sumInterior = copyReviews.reduce((acc, review) => acc + review.rating.interior, 0);
      const sumService = copyReviews.reduce((acc, review) => acc + review.rating.service, 0);
      const sumFood = copyReviews.reduce((acc, review) => acc + review.rating.food, 0);
      const averageInterior = sumInterior === 0 ? 0 : sumInterior / amountReviews;
      const averageService = sumService === 0 ? 0 : sumService / amountReviews;
      const averageFood = sumFood === 0 ? 0 : sumFood / amountReviews;

      place.rating = {
        interior: averageInterior,
        service: averageService,
        food: averageFood,
        overall: ((averageInterior + averageService + averageFood) / 3).toFixed(1)
      };
      await place.save();

      const reviews = await Review.find().populate('user');
      return res.status(200).send({reviews});
    } catch (e) {
      return res.status(500).send({message: 'Ошибка при удалении, попробуйте позже'});
    }

  });

  return router;
};

module.exports = createRouter;
