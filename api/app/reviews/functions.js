module.exports = {
  sumRatings: reviews => {
    return reviews.reduce((acc, review) => {
      const {_id, ...rating} = review.rating._doc;
      const keys = Object.keys(rating);
      keys.forEach(key => (
        acc[key] ? acc[key] += rating[key] : acc[key] = rating[key]
      ));
      return acc;
    }, {});
  },
  averageRating: (sum, amountReviews) => {
    const keys = Object.keys(sum);
    return keys.reduce((acc, key) => {
      sum[key] === 0 ? acc[key] = 0 : acc[key] = sum[key] / amountReviews;
      return acc;
    }, {});
  }
};
