const express = require('express');
const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');

const auth = require('../middleware/auth');
const permit = require('../middleware/permit');
const config = require('../config');
const Place = require('../models/Place');
const Review = require('../models/Review');
const Image = require('../models/Image');
const User = require('../models/User');

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});
const upload = multer({storage});

const createRouter = () => {
  const router = express.Router();

  router.post('/', [auth, upload.single('image')], async (req, res) => {
    const { isAgree, title, description } = req.body;

    if (isAgree) {
      const placeData = {
        user: req.user._id,
        title: title,
        description: description,
        photo: req.file.filename
      };

      const place = new Place(placeData);
      place.rating = {};
      await place.save();

      return res.status(200).send({place});
    }
  });

  router.get('/', async (req, res) => {
    const places = await Place.find()
      .populate('images user')
      .populate({path: 'reviews', populate: {path: 'user'}});
    return res.status(200).send({places});
  });

  router.get('/:id', async (req, res) => {
    const placeId = req.params.id;
    const place = await Place.findById(placeId)
      .populate('images')
      .populate({path: 'reviews', populate: {path: 'user'}});
    const reviews = place.reviews;
    const amountReviews = reviews.length;
    const sumInterior = reviews.reduce((acc, review) => acc + review.rating.interior, 0);
    const sumService = reviews.reduce((acc, review) => acc + review.rating.service, 0);
    const sumFood = reviews.reduce((acc, review) => acc + review.rating.food, 0);
    const averageInterior = sumInterior === 0 ? 0 : sumInterior / amountReviews;
    const averageService = sumService === 0 ? 0 : sumService / amountReviews;
    const averageFood = sumFood === 0 ? 0 : sumFood / amountReviews;

    place.rating = {
      interior: averageInterior,
      service: averageService,
      food: averageFood,
      overall: ((averageInterior + averageService + averageFood) / 3).toFixed(1)
    };
    await place.save();

    return res.status(200).send({place});
  });

  router.delete('/:id', [auth, permit('admin')], async (req, res) => {
    const placeId = req.params.id;
    await Place.findByIdAndDelete(placeId);
    await Review.deleteMany({place: placeId});
    await Image.deleteMany({place: placeId});
    await User.update({}, {$pull: {places: {$in: placeId}}});
    const places = await Place.find();
    return res.status(200).send({places});
  });

  return router;
};

module.exports = createRouter;
