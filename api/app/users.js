const express = require('express');

const User = require('../models/User');

const createRouter = () => {
  const router = express.Router();

  router.post('/', async (req, res) => {
    const userData = new User({
      username: req.body.username,
      password: req.body.password
    });

    try {
      await userData.save();
      return res.send({message: 'Register success!'});
    } catch (err) {
      return res.status(400).send(err);
    }
  });

  router.post('/sessions', async (req, res) => { // +
    const user = await User.findOne({username: req.body.username});

    if (!user) {
      return res.status(400).send({error: 'Username not found'});
    }

    const isMatch = await user.checkPassword(req.body.password);

    if (!isMatch) {
      return res.status(400).send({error: 'Password is wrong!'});
    }

    user.generateToken();
    await user.save();

    return res.send(user);
  });

  router.delete('/sessions', async (req, res) => { // +
    const token = req.get('Token');
    const success = {message: 'Logout success!'};

    if (!token) return res.send(success);

    const user = await User.findOne({token});

    if (!user) return res.send(success);

    user.generateToken();
    await user.save();

    return res.send(success);
  });

  return router;
};

module.exports = createRouter;