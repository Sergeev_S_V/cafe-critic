const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');

const config = require('./config');
const users = require('./app/users');
const places = require('./app/places');
const images = require('./app/images');
const reviews = require('./app/reviews/reviews');

const app = express();

const port = 8000;

app.use(cors());
app.use(express.json());
app.use(express.static('public'));

mongoose.connect(config.db.url + '/' + config.db.name, {useNewUrlParser: true});

const db = mongoose.connection;

db.once('open', () => {
  console.log('Mongoose connected!');

  app.use('/api/users', users());
  app.use('/api/places', places());
  app.use('/api/images', images());
  app.use('/api/reviews', reviews());

  app.listen(port, () => {
    console.log(`Server started on ${port} port!`);
  });
});
