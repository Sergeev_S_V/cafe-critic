const mongoose = require('mongoose');
const moment = require('moment');

const config = require('./config');
const User = require('./models/User');
const Place = require('./models/Place');
const Review = require('./models/Review');

mongoose.connect(config.db.url + '/' + config.db.name);

const db = mongoose.connection;

db.once('open', async () => {
  try {
    await db.dropCollection('users');
    await db.dropCollection('places');
    await db.dropCollection('reviews');
    await db.dropCollection('images');
  } catch (e) {
    console.log('Collections were not present, skipping drop...');
  }

  const [User1, User2, User3] = await User.create({
    username: 'user1',
    password: '123',
    role: 'user'
  }, {
    username: 'user2',
    password: '123',
    role: 'user'
  }, {
    username: 'admin',
    password: '123',
    role: 'admin'
  });

  const [Place1, Place2] = await Place.create({
    user: User1._id,
    title: 'Барашек',
    description: 'Long long long long long long long long description',
    photo: 'resto1.jpg',
    reviews: [],
    rating: {
      food: 3,
      service: 3,
      interior: 4,
      overall: (3 + 3 + 4) / 3
    }
  }, {
    user: User2._id,
    title: 'Москва',
    description: 'Long description long description long description long long long long long description',
    photo: 'resto2.jpg',
    reviews: [],
    rating: {
      food: 5,
      service: 5,
      interior: 4,
      overall: (5 + 5 + 4) / 3
    }
  });

 // const [Review1, Review2, Review3, Review4] = await Review.create({
 //    date: moment().format("DD.MM.YYYY в HH:mm"),
 //    place: Place1._id,
 //    user: User1._id,
 //    review: 'comment 1 comment 1 comment 1 comment 1 comment 1',
 //    rating: {
 //      food: 3,
 //      service: 3,
 //      interior: 4,
 //      overall: (3 + 3 + 4) / 3
 //    }
 //  }, {
 //    date: moment().format("DD.MM.YYYY в HH:mm"),
 //    place: Place1._id,
 //    user: User2._id,
 //    review: 'comment 4 comment 4 comment 4 comment 4 comment 4',
 //    rating: {
 //      food: 4,
 //      service: 4,
 //      interior: 5,
 //      overall: (4 + 4 + 5) / 3
 //    }
 //  }, {
 //    date: moment().format("DD.MM.YYYY в HH:mm"),
 //    place: Place2._id,
 //    user: User2._id,
 //    review: 'comment 3 comment 3 comment 3 comment 3 comment 3',
 //    rating: {
 //      food: 1,
 //      service: 2,
 //      interior: 3,
 //      overall: (1 + 2 + 3) / 3
 //    }
 //  }, {
 //    date: moment().format("DD.MM.YYYY в HH:mm"),
 //    place: Place2._id,
 //    user: User2._id,
 //    review: 'comment 2 comment 2 comment 2 comment 2 comment 2',
 //    rating: {
 //      food: 2,
 //      service: 3,
 //      interior: 3,
 //      overall: (2 + 3 + 3) / 3
 //    }
 //  });
 //
 // Place1.reviews.push(Review1);
 // Place1.reviews.push(Review2);
 // Place2.reviews.push(Review3);
 // Place2.reviews.push(Review4);


 db.close();
});