const path = require('path');

const rootPath = __dirname;

module.exports = {
  rootPath,
  uploadPath: path.join(rootPath, '/public/uploads'),
  db: {
    url: 'mongodb://localhost:27017',
    name: 'exam-13'
  },
  facebook: {
    appId: "1810552955671073",
    appSecret: "10edc96c2214e775cc81b11be4ef38c3"
  }
};

