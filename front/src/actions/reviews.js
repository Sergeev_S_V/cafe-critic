import axios from '../axios-config';
import {
  DELETE_REVIEW_FAILURE, DELETE_REVIEW_SUCCESS, SEND_REVIEW_FAILURE,
  SEND_REVIEW_SUCCESS
} from "../constants/actionTypes";
import {NotificationManager} from "react-notifications";

export const sendReview = reviewData => async dispatch => {
  try {
    const res = await axios.post('/reviews', reviewData);
    const { data: { place } } = res;
    dispatch(sendReviewSuccess(place));
  } catch (error) {
  	const { data, status } = error.response;
	  if (status === 403) {
	    dispatch(sendReviewFailure(data));
	  }
	  if (status === 400) {
		  NotificationManager.error(data.message);
	  }
  }
};
const sendReviewSuccess = updatedPlace => ({
  type: SEND_REVIEW_SUCCESS, updatedPlace
});
const sendReviewFailure = error => ({
  type: SEND_REVIEW_FAILURE, error
});

export const deleteReview = reviewId => async dispatch => {
  try {
    const res = await axios.delete(`/reviews/${reviewId}`);
    const { status, data: {reviews}} = res;
    if (status === 200) {
      dispatch(deleteReviewSuccess(reviews));
    }
  } catch (error) {
    dispatch(deleteReviewFailure(error.response.data));
    NotificationManager.error(error.response.data.message);
  }
};

const deleteReviewSuccess = reviews => ({
  type: DELETE_REVIEW_SUCCESS, reviews
});

const deleteReviewFailure = error => ({
  type: DELETE_REVIEW_FAILURE, error
});
