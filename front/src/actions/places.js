import axios from '../axios-config';
import {
  ADD_NEW_PLACE_SUCCESS, DELETE_PLACE_SUCCESS, FETCH_ONE_PLACE_SUCCESS,
  FETCH_PLACES_SUCCESS
} from "../constants/actionTypes";
import {push} from "react-router-redux";
import {removeImage, uploadNewImage} from "./images";
import {deleteReview, sendReview} from "./reviews";


export const addNewPlace = placeData => async dispatch => {
  try {
    const res = await axios.post('/places', placeData);
    dispatch(addNewPlaceSuccess(res.data.place));
    dispatch(push('/'));
  } catch (e) {

  }
};

const addNewPlaceSuccess = place => ({
  type: ADD_NEW_PLACE_SUCCESS, place
});



export const fetchPlaces = () => async dispatch => {
  try {
    const res = await axios.get('/places');
    const { places } = res.data;
    dispatch(fetchPlacesSuccess(places));
  } catch (e) {

  }
};

const fetchPlacesSuccess = places => ({
  type: FETCH_PLACES_SUCCESS, places
});


export const fetchOnePlace = placeId => async dispatch => {
  try {
    const res = await axios.get(`/places/${placeId}`);
    const { place } = res.data;
    dispatch(fetchOnePlaceSuccess(place));
  } catch (e) {

  }
};

const fetchOnePlaceSuccess = place => ({
  type: FETCH_ONE_PLACE_SUCCESS, place
});



export const deletePlace = placeId => async dispatch => {
  try {
    const res = await axios.delete(`/places/${placeId}`);
    const { places } = res.data;
    dispatch(deletePlaceSuccess(places));
  } catch (e) {
    
  }
};

const deletePlaceSuccess = places => ({
  type: DELETE_PLACE_SUCCESS, places
});


export default {
  deletePlace,
  fetchOnePlace,
  fetchPlaces,
  addNewPlace,
  uploadNewImage,
  sendReview,
  deleteReview,
  removeImage
}

