import {NotificationManager} from "react-notifications";

import axios from '../axios-config';
import {
	REMOVE_IMAGE_FAILURE, REMOVE_IMAGE_SUCCESS, UPLOAD_NEW_IMAGE_FAILURE,
	UPLOAD_NEW_IMAGE_SUCCESS
} from "../constants/actionTypes";

export const uploadNewImage = imageData => async dispatch => {
  try {
    const res = await axios.post('/images', imageData);
    const { image, message } = res.data;
    dispatch(uploadNewImageSuccess(image));
	  NotificationManager.success(message);
  } catch (e) {
	  dispatch(uploadNewImageFailure(e.response.data.error));
	  NotificationManager.error(e.response.data.error);
  }
};

const uploadNewImageSuccess = image => ({
  type: UPLOAD_NEW_IMAGE_SUCCESS, image
});

const uploadNewImageFailure = error => ({
	type: UPLOAD_NEW_IMAGE_FAILURE, error
});

export const removeImage = imageId => async dispatch => {
  try {
    const res = await axios.delete(`/images/${imageId}`);
    const { data: { images, message }, status } = res;
    if (status === 200) {
      dispatch(removeImageSuccess(images));
	    NotificationManager.success(message);
    }
  } catch (e) {
	  dispatch(removeImageFailure(e.response.data.error));
	  NotificationManager.error(e.response.data.error);
  }
};
const removeImageSuccess = images => ({
  type: REMOVE_IMAGE_SUCCESS, images
});
const removeImageFailure = error => ({
	type: REMOVE_IMAGE_FAILURE, error
});