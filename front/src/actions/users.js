import axios from "../../src/axios-config";
import {push} from "react-router-redux";
import {NotificationManager} from "react-notifications";

import {
  LOGIN_USER_FAILURE, LOGIN_USER_SUCCESS, LOGOUT_USER,
  REGISTER_USER_FAILURE,
  REGISTER_USER_SUCCESS,
} from "../constants/actionTypes";

const registerUser = userData => {
  return dispatch => {
    return axios.post('/users', userData).then(
      (response) => {
        dispatch(registerUserSuccess());
        dispatch(push('/'));
        NotificationManager.success('Success', response.data.message);
      },
      error => {
        dispatch(registerUserFailure(error.response.data));
      }
    );
  };
};

const registerUserSuccess = () => {
  return {type: REGISTER_USER_SUCCESS};
};

const registerUserFailure = error => {
  return {type: REGISTER_USER_FAILURE, error};
};

const loginUser = userData => dispatch => {
  return axios.post('/users/sessions', userData)
    .then(res => {
        dispatch(loginUserSuccess(res.data));
        dispatch(push('/'));
        NotificationManager.success('Success', 'Login success');
      },
      error => {
        const err = error.response ? error.response.data : {error: 'No internet connection'};
        dispatch(loginUserFailure(err));
      });
};

const loginUserSuccess = (user) => {
  return {type: LOGIN_USER_SUCCESS, user};
};

const loginUserFailure = (error) => {
  return {type: LOGIN_USER_FAILURE, error};
};

const logoutUser = () => async dispatch => {
  const response = await axios.delete('/users/sessions');
  dispatch(push('/'));
  dispatch(logoutUserSuccess());
  NotificationManager.success('Success', response.data.message);
};

const logoutUserSuccess = () => ({
  type: LOGOUT_USER
});

export const logoutExpiredUser = () => dispatch => {
  dispatch(push('/'));
  dispatch(logoutUserSuccess());
  NotificationManager.error('Error', 'Your session has expired, please login again');
};

export default {
  loginUser,
  logoutUser,
  registerUser
}