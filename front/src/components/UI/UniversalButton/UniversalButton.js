import React from 'react';
import {Button, Col, FormGroup} from "react-bootstrap";

const UniversalButton = ({ text }) => (
  <FormGroup>
    <Col smOffset={2} sm={10}>
      <Button bsStyle="primary" type="submit">
        {text}
      </Button>
    </Col>
  </FormGroup>
);

export default UniversalButton;