import React from 'react';
import {LinkContainer} from "react-router-bootstrap";
import Exit from 'react-icons/lib/md/exit-to-app';


const UserMenu = ({user, logout}) => {
  return (
    <div className='header__user-menu'>
      <LinkContainer to='/places/add_new'>
        <a href={null} className='header__link'>Добавить новое место</a>
      </LinkContainer>
      <span className='header__hello-user'>Привет, {user.username}!</span>
      <Exit className='header__logout-btn' onClick={logout} />
    </div>
  )
};

export default UserMenu;