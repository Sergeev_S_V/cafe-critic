import React from 'react';
import {LinkContainer} from "react-router-bootstrap";

const AnonymousMenu = () => (
  <ul className='header__anonymous-menu'>
    <li className='header__list-item'>
      <LinkContainer to="/register" exact>
        <a href={null} className='header__link'>Регистрация</a>
      </LinkContainer>
    </li>
    <li className='header__list-item'>
      <LinkContainer to="/login" exact>
        <a href={null} className='header__link'>Войти</a>
      </LinkContainer>
    </li>
  </ul>
);

export default AnonymousMenu;