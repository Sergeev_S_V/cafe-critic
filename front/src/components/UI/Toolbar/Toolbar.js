import React from 'react';
import {LinkContainer} from "react-router-bootstrap";

import AnonymousMenu from "../Menus/AnonymousMenu";
import UserMenu from "../Menus/UserMenu";
import './Toolbar.css';

const Toolbar = ({user, logout}) => (
  <nav className='header__toolbar'>
    <LinkContainer to="/" exact>
      <a href={null} className='header__site-name'>Кафе критик</a>
    </LinkContainer>
    {user
      ? <UserMenu logout={logout} user={user}/>
      : <AnonymousMenu/>
    }
  </nav>
);


export default Toolbar;