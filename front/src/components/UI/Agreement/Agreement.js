import React from 'react';
import {Checkbox, Col, FormGroup} from "react-bootstrap";

const Agreement = ({ onAccept }) => (
  <FormGroup>
    <Col md={12}>
      <h6 style={{textAlign: 'center'}}>
        <strong>Добавляя новое заведение, вы соглашаетесь с условиями соглашения при создании нового заведения.</strong>
      </h6>
      <p className='add-new-place-form__label_align'>
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium amet at atque consequatur earum facere, facilis fuga iusto laboriosam magnam maiores maxime mollitia necessitatibus nihil nobis nulla optio porro praesentium quaerat quo rerum saepe sapiente sequi, suscipit, voluptatibus! Corporis delectus ducimus excepturi modi molestiae obcaecati perferendis quasi, rem voluptates voluptatum. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequatur ducimus ea eius eos libero quae quaerat quas sit temporibus voluptates. Aperiam architecto dignissimos, doloribus iste modi nemo neque, obcaecati perferendis quaerat saepe sapiente sint sunt. At excepturi facere fuga illum ipsum laudantium libero officia omnis porro, quam rem saepe voluptas.
      </p>
      <Checkbox onChange={ onAccept } style={{textAlign: 'center'}}>
        Я согласен
      </Checkbox>
    </Col>
  </FormGroup>
);

export default Agreement;