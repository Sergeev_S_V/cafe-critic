import React from 'react';
import './styles.css';

const Warning = ({ text }) => (
  <span className='warning'>{text}</span>
);

export default Warning;