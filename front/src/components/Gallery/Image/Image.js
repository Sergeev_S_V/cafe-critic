import React from 'react';
import DeleteIcon from 'react-icons/lib/md/delete-forever';

import paths from '../../../constants/paths';

const Image = ({ id, image, user, remove })  => (
  <div className='images-list__image-block'>
    <img className='images-list__image' src={paths.uploadPathImage + image} alt=""/>
    {user && user.role === 'admin' &&
      <DeleteIcon className='delete-icon' onClick={() => remove(id)}/>
    }
  </div>
);

export default Image;
