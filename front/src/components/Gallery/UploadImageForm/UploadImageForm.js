import React, {Fragment} from 'react';
import {Button, FormControl, FormGroup} from "react-bootstrap";
import { withStateHandlers } from 'recompose';


const initialState = {
	image: ''
};

const handlers = {
	fileChangeHandler: () => e => ({
		[e.target.name]: e.target.files[0]
	}),
	uploadNewImageHandler: (state, props) => () => {
		const formData = new FormData();
    Object.keys(state).forEach(key => {
      formData.append(key, state[key]);
    });
    const placeId = props.placeId;
    formData.append('placeId', placeId);

    props.upload(formData);

    return {
    	image: ''
    };
	}
};

const UploadImageForm = ({ fileChangeHandler, uploadNewImageHandler }) => (
	<Fragment>
		<h3>Добавить новое фото</h3>

		<FormGroup controlId="uploadImage">
			<FormControl
				type="file"
				name="image"
				onChange={ fileChangeHandler }
			/>
		</FormGroup>

		<FormGroup>
			<Button bsStyle="primary" onClick={ uploadNewImageHandler }>
				Добавить
			</Button>
		</FormGroup>

	</Fragment>
);

const enhance = withStateHandlers(initialState, handlers);
export default enhance(UploadImageForm);