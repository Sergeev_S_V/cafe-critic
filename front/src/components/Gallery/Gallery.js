import React from 'react';

import ImagesList from "./ImagesList/ImagesList";
import './styles.css';

const Gallery = ({ images, user, remove }) => (
  <div className='gallery'>
    <h4 className='gallery__title'>Галлерея</h4>
    {images.length > 0
      ? <ImagesList images={images} user={user} remove={remove}/>
      : <h5>У заведения ещё нет фотографий</h5>
    }
  </div>
);

export default Gallery;