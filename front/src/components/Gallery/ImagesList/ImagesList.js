import React from 'react';

import Image from "../Image/Image";

const ImagesList = ({ images, user, remove }) => (
  <div className='images-list'>
    {images.map(image => (
      <Image key={image._id} image={image.image} user={user} remove={remove} id={image._id} />
    ))}
  </div>
);


export default ImagesList;