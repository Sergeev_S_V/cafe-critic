import React from 'react';

const Review = ({ review }) => (
  <div className='review-block__review'>
    <div className='review-block__review-header'>
      <span className='review-block__date'>Отправлено: {review.date}</span>
      <span className='review-block__reviewer'>{review.user.username}</span>
    </div>
    <p className='review-block__text-review'>{review.review}</p>
  </div>
);

export default Review;