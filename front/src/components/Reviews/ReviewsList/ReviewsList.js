import React from 'react';
import Trashcan from 'react-icons/lib/md/delete-forever';

import RatingsList from "../../Ratings/RatingsList/RatingsList";
import Review from "../Review/Review";
import './styles.css';

const ReviewsList = ({ reviews, user, remove, error }) => (
  <div className='reviews'>
    <h4 className='reviews__title'>Отзывы</h4>
    {reviews.length > 0
      ? reviews.map(review => (
          <div key={review._id} className='review-block'>
            <div className='review-block__header'>
              <Review review={review} />
              {error && review._id === error.reviewId
                && <Trashcan className='trashcan' onClick={() => remove(error.reviewId)} />
              }
              {user && user.role === 'admin'
                && <Trashcan className='trashcan' onClick={() => remove(review._id)} />
              }
            </div>
            <RatingsList
              rating={review.rating}
              isOnlyRead={true}
            />
          </div>
      ))
      : <h4>Об этом заведении ещё нет отзывов</h4>
    }
  </div>
);

export default ReviewsList;