import React, {Component, Fragment} from 'react';
import {Button, Col, FormControl, FormGroup, HelpBlock} from "react-bootstrap";
import Rating from "react-rating";

import greyStar from '../../../assets/grey-star.png';
import yellowStar from '../../../assets/yellow-star.png';


class AddReviewForm extends Component {

  state = {
    interior: 0,
    service: 0,
    food: 0,
    review: ''
  };

  inputChangeHandler = e => {
    this.setState({[e.target.name]: e.target.value});
  };

  setRatingHandler = (value, field) => {
	  this.setState({[field]: value});
  };

  sendReviewHandler = () => {
    const { food, service, interior, review } = this.state;
    const { placeId, sendReview } = this.props;

    if (food && service && interior !== 0 && review.length > 0) {
      const reviewData = {
        placeId: placeId,
        review: review,
        food: food,
        service: service,
        interior: interior
      };

      sendReview(reviewData);
      this.clearState();
    }
  };

  clearState = () => {
    this.setState({interior: 0, service: 0, food: 0, review: ''});
  };

  render() {
    const { review, food, service, interior } = this.state;
    const { ratingError } = this.props;

    return(
      <Fragment>
        <h3>Добавить отзыв</h3>
        <HelpBlock>{ratingError && ratingError.message}</HelpBlock>

        <FormGroup controlId="reviewText">
          <FormControl
            componentClass="textarea"
            onChange={this.inputChangeHandler}
            name='review'
            placeholder="Enter review"
            value={review}
          />
        </FormGroup>
        <Col md={10}>
          <Col md={3}>
            <span>Еда</span>
            <Rating
              onChange={(value) => {this.setRatingHandler(value, 'food')}}
              initialRating={food}
              emptySymbol={<img src={greyStar} alt=""/>}
              fullSymbol={<img src={yellowStar} alt=""/>}
            />
          </Col>
          <Col md={3}>
            <span>Сервис</span>
            <Rating
              onChange={(value) => {this.setRatingHandler(value, 'service')}}
              initialRating={service}
              emptySymbol={<img src={greyStar} alt=""/>}
              fullSymbol={<img src={yellowStar} alt=""/>}
            />
          </Col>
          <Col md={3}>
            <span>Интерьер</span>
            <Rating
              onChange={(value) => {this.setRatingHandler(value, 'interior')}}
              initialRating={interior}
              emptySymbol={<img src={greyStar} alt=""/>}
              fullSymbol={<img src={yellowStar} alt=""/>}
            />
          </Col>
        </Col>
        <Col md={2}>
          <FormGroup>
            <Button bsStyle="primary" onClick={this.sendReviewHandler}>
              Отправить отзыв
            </Button>
          </FormGroup>
        </Col>
      </Fragment>
    );
  }
}

export default AddReviewForm;