import React from 'react';
import Overall from "./Overall/Overall";
import './styles.css';

const Rating = ({ rating }) => (
  <div className='rating-block'>
    <h4 className='rating-block__title'>Рейтинг заведения</h4>
    <Overall
      rating={rating}
      isOnlyRead={true}
    />
  </div>
);

export default Rating;