import React from 'react';
import Ratings from "../Ratings/Ratings";
import './styles.css';

const RatingsList = ({ rating, isOnlyRead }) => (
  <div className='ratings-list'>
    <Ratings isOnlyRead={isOnlyRead} title='Еда' value={rating && rating.food}  />
    <Ratings isOnlyRead={isOnlyRead} title='Сервис' value={rating && rating.service}  />
    <Ratings isOnlyRead={isOnlyRead} title='Интерьер' value={rating && rating.interior} />
  </div>
);

export default RatingsList;