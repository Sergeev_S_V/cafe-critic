import React from 'react';
import Stars from "react-rating";

import greyStar from '../../../assets/grey-star.png';
import yellowStar from '../../../assets/yellow-star.png';

const Ratings = ({ title, value, isOnlyRead }) => (
  <div className='ratings-list__ratings'>
    {title && <span>{title}</span>}
    <Stars
      readonly={isOnlyRead}
      initialRating={value}
      emptySymbol={<img src={greyStar} alt=""/>}
      fullSymbol={<img src={yellowStar} alt=""/>}
    />
  </div>
);

export default Ratings;