import React, {Fragment} from 'react';
import {Panel} from "react-bootstrap";
import {lifecycle} from "recompose";

import AddReviewForm from "../../Reviews/AddReviewForm/AddReviewForm";
import UploadImageForm from "../../Gallery/UploadImageForm/UploadImageForm";
import Rating from "../../../components/Ratings/Rating/Rating";
import Gallery from "../../../components/Gallery/Gallery";
import ReviewsList from "../../../components/Reviews/ReviewsList/ReviewsList";
import '../../../components/Places/ViewPlace/styles.css';
import paths from '../../../constants/paths';


const hooks = {
	componentDidMount() {
		const placeId = this.props.match.params.id;
		this.props.actions.fetchOnePlace(placeId);
	}
};

const ViewPlace = ({
	place,
	user,
	ratingError,
	actions: {deleteReview, sendReview, uploadNewImage, removeImage}
}) => (
	<Fragment>
		{place &&
			<div>
				<div className='place__header'>
					<img src={paths.uploadPathImage + place.photo}
					     alt=""
					     className='place__main-photo'
					/>
					<div className='place__header-item'>
						<h1>{place.title}</h1>
						<h4>Описание заведения</h4>
						<p className='place__description'>
							{place.description} Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi beatae consectetur eaque, iure nam numquam totam. Architecto, eos maiores neque officiis repellat tempore! Accusantium alias atque autem cupiditate debitis dicta, dolore doloribus eaque eius eligendi error eveniet explicabo fugiat, ipsa ipsum iusto mollitia numquam perferendis placeat quaerat recusandae, repellendus rerum sed vel vitae! Aut autem dolor explicabo facere laboriosam perferendis reiciendis unde. Aliquid, aperiam beatae delectus dicta dolorem eligendi eveniet fugiat, id ipsam laborum maiores, nostrum nulla officiis qui sapiente sint soluta suscipit tempore totam voluptatum. Accusamus asperiores aspernatur autem consectetur culpa cumque cupiditate deserunt dicta ea eaque error esse et facere fuga fugit id magni modi mollitia natus non nostrum optio quas qui quia recusandae reprehenderit repudiandae soluta ullam vero, voluptatem! Adipisci aliquid, atque debitis nemo non soluta voluptate? Aliquid corporis dolore doloribus ea est fugit harum iure laborum magnam minima minus modi molestiae nisi nostrum obcaecati odit officia perspiciatis quod ratione reiciendis ullam velit, vitae. Doloremque eligendi id magnam officiis quod reiciendis.
						</p>
					</div>
				</div>
				<div className='place__body'>
					<Gallery images={place.images} user={user} remove={removeImage}/>
					<Rating rating={place.rating} />
					<ReviewsList
						reviews={place.reviews}
						user={user}
						error={ratingError}
						remove={deleteReview}
					/>
					<div>
						{user
							? <AddReviewForm placeId={place._id} ratingError={ratingError} sendReview={sendReview} />
							: <h3>Не зарегистрированные пользователи немогут оставлять отзывы и ставить оценку</h3>
						}
					</div>
				</div>
				{user &&
					<Panel.Footer>
						<UploadImageForm placeId={place._id} upload={uploadNewImage}/>
					</Panel.Footer>
				}
			</div>
		}
	</Fragment>
);

export default lifecycle(hooks)(ViewPlace);