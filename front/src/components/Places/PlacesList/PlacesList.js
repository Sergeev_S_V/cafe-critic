import React from 'react';
import { lifecycle } from 'recompose';
import {Link} from "react-router-dom";
import Stars from "react-rating";
import Trashcan from 'react-icons/lib/md/delete';
import Camera from 'react-icons/lib/md/photo-camera';
import Comment from 'react-icons/lib/md/comment';

import greyStar from '../../../assets/grey-star.png';
import yellowStar from '../../../assets/yellow-star.png';
import paths from '../../../constants/paths';
import './PlacesList.css';


const hooks = {
	componentDidMount() {
		this.props.actions.fetchPlaces();
	}
};

const PlacesList = ({ places, user, actions: { deletePlace } }) => (
	<div className='places-list'>
		{places.length > 0
			? places.map(place => (
				<div key={place._id} className='preview-place'>
					<Link to={`/places/${place._id}`} className='preview-place__link'>
						<img className='preview-place__image' src={paths.uploadPathImage + place.photo} alt=""/>
						<h6 className='preview-place__title'>{place.title}</h6>
					</Link>
					<div className='preview-place__rating'>
						<div className='preview-place__stars'>
							<Stars
								readonly
								initialRating={place.rating.overall}
								emptySymbol={<img src={greyStar} alt=""/>}
								fullSymbol={<img src={yellowStar} alt=""/>}
							/>
						</div>
						<span className='preview-place__rating-text'>
	            <span><Camera className='preview-place__photo-camera'/> {place.images.length} </span>
	            <span><Comment className='preview-place__comment-svg'/> {place.reviews.length} </span>
            </span>
						{user && user.role === 'admin' &&
							<Trashcan className='preview-place__delete-btn' onClick={() => deletePlace(place._id)}>
								Удалить
							</Trashcan>
						}
					</div>
				</div>
			))

			: <h3>Ни одного заведения ещё не добавили</h3>

		}
	</div>
);

export default lifecycle(hooks)(PlacesList);

