import React, {Component, Fragment} from 'react';
import {
	Button, Col, ControlLabel, Form, FormControl, FormGroup, HelpBlock, PageHeader
} from "react-bootstrap";

import './AddNewPlace.css';
import Agreement from "../../../components/UI/Agreement/Agreement";


class AddNewPlace extends Component {

	state = {
		title: '',
		description: '',
		image: '',
		isAgree: false,
		agreeError: ''
	};

	componentDidMount() {
		const isAgree = localStorage.getItem('isAgree');
		if (isAgree !== null) {
			this.setState({ isAgree });
		}
	}

	inputChangeHandler = event => {
		this.setState({[event.target.name]: event.target.value});
	};

	fileChangeHandler = event => {
		this.setState({[event.target.name]: event.target.files[0]});
	};

	submitFormHandler = e => {
		e.preventDefault();
		const { isAgree } = this.state;
		const { actions: { addNewPlace } } = this.props;

		if (isAgree === false) {
			return this.setState({agreeError: 'Чтобы добавлять заведения, нужно принять соглашение'});
		}

		const formData = new FormData();
		Object.keys(this.state).forEach(key => {
			if (key !== 'agreeError') {
				formData.append(key, this.state[key]);
			}
		});

		addNewPlace(formData);
	};

	acceptAgreement = () => {
		localStorage.setItem('isAgree', 'true');
		this.setState({isAgree: true, agreeError: ''});
	};

	render() {
		const { isAgree, agreeError, title, description } = this.state;

		return(
			<Fragment>
				<PageHeader>Add new place</PageHeader>
				<Form horizontal onSubmit={this.submitFormHandler}>
					<FormGroup controlId="placeTitle">
						<Col componentClass={ControlLabel} sm={2}>
							Title
						</Col>
						<Col sm={10}>
							<FormControl
								type="text"
								required
								placeholder="Enter title"
								name="title"
								value={title}
								onChange={this.inputChangeHandler}
							/>
						</Col>
					</FormGroup>

					<FormGroup controlId="placeDescription">
						<Col componentClass={ControlLabel} sm={2}>
							Description
						</Col>
						<Col sm={10}>
							<FormControl
								type="text"
								required
								placeholder="Enter description"
								name="description"
								value={description}
								onChange={this.inputChangeHandler}
							/>
						</Col>
					</FormGroup>

					<FormGroup controlId="photoImage">
						<Col componentClass={ControlLabel} sm={2}>
							Image
						</Col>
						<Col sm={10}>
							<FormControl
								type="file"
								required
								name="image"
								onChange={this.fileChangeHandler}
							/>
						</Col>
					</FormGroup>

					{ !isAgree &&
					<Agreement onAccept={this.acceptAgreement}/>
					}

					<FormGroup>
						<Col smOffset={2} md={2}>
							<Button bsStyle="primary" type="submit">Add place</Button>
						</Col>

						{agreeError.length > 0 &&
						<Col md={8}>
							<strong><HelpBlock>{agreeError}</HelpBlock></strong>
						</Col>
						}
					</FormGroup>
				</Form>
			</Fragment>
		);
	}
}

export default AddNewPlace;