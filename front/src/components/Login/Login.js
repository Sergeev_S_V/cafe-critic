import React, {Fragment} from 'react';
import {Alert, Form, PageHeader} from "react-bootstrap";
import { withStateHandlers } from 'recompose';

import FormElement from "../../components/UI/FormElement/FormElement";
import UniversalButton from '../../components/UI/UniversalButton/UniversalButton';


const initialState = {
	username: '',
	password: ''
};

const handlers = {
	onChange: () => e => ({
    [e.target.name]: e.target.value
  }),
  onSubmit: (state, props) => e => {
		e.preventDefault();
    props.actions.loginUser(state);
  }
};

const Login = ({ username, password, error, onChange, onSubmit }) => (
  <Fragment>
    <PageHeader>Login</PageHeader>
    <Form horizontal onSubmit={ onSubmit }>
			{error &&
        <Alert bsStyle='danger'>{error.error}</Alert>
			}

      <FormElement propertyName='username'
                   title='Username'
                   placeholder='Enter username'
                   type='text'
                   autoComplete='current-username'
                   value={username}
                   changeHandler={ onChange }
                   required
      />

      <FormElement propertyName='password'
                   title='Password'
                   placeholder='Enter password'
                   type='password'
                   autoComplete='current-password'
                   value={password}
                   changeHandler={ onChange }
                   required
      />

      <UniversalButton text="Login"/>
    </Form>
  </Fragment>
);

const enhance = withStateHandlers(initialState, handlers);
export default enhance(Login);

