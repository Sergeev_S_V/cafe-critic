import React, {Fragment} from 'react';
import {NotificationContainer} from 'react-notifications';
import 'react-notifications/lib/notifications.css';

import Toolbar from "../../components/UI/Toolbar/Toolbar";
import './Layout.css';

const Layout = ({children, user, actions: {logoutUser}}) => (
  <Fragment>
    <NotificationContainer />
    <header className='header'>
      <div className='container'>
        <Toolbar user={user} logout={logoutUser}/>
      </div>
    </header>
    <main>
      <div className='container'>
        {children}
      </div>
    </main>
  </Fragment>
);

export default Layout;