import {combineReducers} from "redux";
import {routerReducer as routing} from "react-router-redux";
import users from "./users";
import places from "./places";

export const rootReducer = combineReducers({
  users,
  places,
  routing
});