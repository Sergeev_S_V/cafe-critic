import {
	ADD_NEW_PLACE_SUCCESS, DELETE_PLACE_SUCCESS, DELETE_REVIEW_FAILURE, DELETE_REVIEW_SUCCESS, FETCH_ONE_PLACE_SUCCESS,
	FETCH_PLACES_SUCCESS, REMOVE_IMAGE_FAILURE, REMOVE_IMAGE_SUCCESS,
	SEND_REVIEW_FAILURE,
	SEND_REVIEW_SUCCESS, UPLOAD_NEW_IMAGE_FAILURE,
	UPLOAD_NEW_IMAGE_SUCCESS
} from "../constants/actionTypes";


const initialState = {
  places: [],
  place: null,
  errors: {
    ratingError: null,
    deleteError: null,
	  uploadImageError: null,
	  removeImageError: null
  }
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_NEW_PLACE_SUCCESS:
      return {...state, places: state.places.concat(action.place)};

    case FETCH_PLACES_SUCCESS:
      return {...state, places: action.places};

    case FETCH_ONE_PLACE_SUCCESS:
      return {...state, place: action.place, errors: {...state.errors, ratingError: null}};

	  case UPLOAD_NEW_IMAGE_SUCCESS:
      const copyPlace = {...state.place};
      copyPlace.images.push(action.image);
      return {...state, place: copyPlace, errors: {...state.errors, uploadImageError: null}};
	  case UPLOAD_NEW_IMAGE_FAILURE:
	  	return {...state, errors: {...state.errors, uploadImageError: action.error}};

    case REMOVE_IMAGE_SUCCESS:
      return {...state,
	      place: {...state.place, images: action.images},
	      errors: {...state.errors, removeImageError: null}
      };
	  case REMOVE_IMAGE_FAILURE:
	  	return {...state, errors: {...state.errors, removeImageError: action.error}};

    case SEND_REVIEW_SUCCESS:
      return {...state, place: action.updatedPlace};
    case SEND_REVIEW_FAILURE:
      return {...state, errors: {...state.errors, ratingError: action.error}};

    case DELETE_PLACE_SUCCESS:
      return {...state, places: action.places};

	  case DELETE_REVIEW_SUCCESS:
      return {...state,
	      place: {...state.place, reviews: action.reviews},
	      errors: {...state.errors, ratingError: null}
      };
    case DELETE_REVIEW_FAILURE:
      return {...state, errors: {deleteError: action.error}};
    default:
      return state;
  }
};

export default reducer;