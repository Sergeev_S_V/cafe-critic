import axios from 'axios';
import paths from './constants/paths';

const instance = axios.create({
  baseURL: paths.api
});

export default instance;