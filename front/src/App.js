import React, { Component } from 'react';
import {connect} from "react-redux";
import {withRouter} from "react-router-dom";

import Routes from "./routes";
import Layout from "./containers/Layout/LayoutContainer";

class App extends Component {
  render() {
    const { user } = this.props;
    return (
      <Layout>
        <Routes user={user}/>
      </Layout>
    );
  }
}

const mapStateToProps = state => ({
  user: state.users.user
});

export default withRouter(connect(mapStateToProps)(App));
