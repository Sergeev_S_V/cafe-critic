const baseUrl = 'http://localhost:8000';

export default {
  api: baseUrl + '/api',
	uploadPathImage: baseUrl + '/uploads/'
}