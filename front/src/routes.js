import React from 'react';
import {Redirect, Route, Switch} from "react-router-dom";
import Register from "./containers/Register/RegisterContainer";
import Login from "./containers/Login/LoginContainer";
import AddNewPlace from "./containers/Places/AddNewPlace/AddNewPlaceContainer";
import PlacesList from "./containers/Places/PlacesList/PlacesListContainer";
import ViewPlace from "./containers/Places/ViewPlace/ViewPlaceContainer";


const ProtectedRoute = ({isAllowed, ...props}) => (
  isAllowed ? <Route {...props}/> : <Redirect to="/login" />
);

const Routes = ({ user }) => (
  <Switch>
    <ProtectedRoute
      isAllowed={user}
      path="/places/add_new"
      exact
      render={() => <AddNewPlace user={user}/>}
    />
    <Route path="/places/:id" exact component={ViewPlace}/>
    <Route path="/" exact component={PlacesList}/>
    <Route path="/places" exact component={PlacesList}/>
    <Route path="/register" exact component={Register}/>
    <Route path="/login" exact component={Login}/>
  </Switch>
);

export default Routes;