import {connect} from "react-redux";
import {bindActionCreators} from "redux";

import actions from "../../../actions/places";
import PlacesList from "../../../components/Places/PlacesList/PlacesList";


const mapStateToProps = state => ({
  places: state.places.places,
  user: state.users.user
});

const mapDispatchToProps = dispatch => ({
	actions: bindActionCreators(actions, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(PlacesList);

