import {connect} from "react-redux";
import {bindActionCreators} from "redux";

import actions from "../../../actions/places";
import AddNewPlace from "../../../components/Places/AddNewPlace/AddNewPlace";


const mapDispatchToProps = dispatch => ({
	actions: bindActionCreators(actions, dispatch)
});

export default connect(null, mapDispatchToProps)(AddNewPlace);