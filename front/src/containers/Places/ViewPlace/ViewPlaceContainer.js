import {connect} from "react-redux";
import {bindActionCreators} from "redux";

import actions from '../../../actions/places';
import ViewPlace from "../../../components/Places/ViewPlace/ViewPlace";


const mapStateToProps = state => ({
  place: state.places.place,
  user: state.users.user,
  ratingError: state.places.errors.ratingError,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(actions, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(ViewPlace);