import {connect} from 'react-redux';
import {bindActionCreators} from "redux";
import Login from "../../components/Login/Login";
import UserActions from '../../actions/users';

const mapStateToProps = state => ({
  error: state.users.loginError
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(UserActions, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(Login);