import {connect} from 'react-redux';
import {bindActionCreators} from "redux";

import UserActions from "../../actions/users";
import Register from "../../components/Register/Register";

const mapStateToProps = state => ({
  error: state.users.registerError
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(UserActions, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(Register);