import {connect} from 'react-redux';
import {bindActionCreators} from "redux";

import actions from "../../actions/users";
import Layout from "../../components/Layout/Layout";


const mapStateToProps = state => ({
  user: state.users.user
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(actions, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(Layout);